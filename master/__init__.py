import logging
import asyncio
import os

from aiohttp import web
from .Master import Master
from .DB import Database
from .config import config


# LOGGING INIT
logging.basicConfig(format="%(levelname)s::%(asctime)s::%(pathname)s::%(funcName)s - %(message)s",
                    datefmt="%Y-%m-%d %a %H:%M %Z",
                    level=logging.WARNING)
# ------------

event_loop = asyncio.get_event_loop()
conn = event_loop.run_until_complete(Database.create_conn(config))
master = Master(conn)


async def on_shutdown(app_):
    tasks = [t for t in asyncio.all_tasks() if t is not
             asyncio.current_task()]

    for task in tasks:
        # skipping over shielded coro still does not help
        if task._coro.__name__ == "cant_stop_me":
            continue
        task.cancel()

    await asyncio.gather(*tasks, return_exceptions=True)
    logging.warning('shutdown')


async def hello(request):
    return web.json_response({'status': 'OK', 'DEBUG': int(os.environ.get('DEBUG', 0))})


app = web.Application()
app.on_shutdown.append(on_shutdown)
app.add_routes([
    web.get(r'/', hello),
    web.get(r'/control/ws_reg', master.ws_reg),
    web.post(r'/control', master.current_instances),
    web.post(r'/control/{id}', master.do),
])
