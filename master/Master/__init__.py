from aiohttp import web, ClientSession
from asyncpg import Connection
from master.DB import Database
from uuid import uuid4
from typing import Dict


class Master:

    def __init__(self, conn: Connection):
        self.slaves = dict()
        self.conn = conn

    async def ws_reg(self, request: web.Request) -> web.WebSocketResponse:
        ws = web.WebSocketResponse()
        await ws.prepare(request)

        async for msg in ws:
            msg = msg.json()  # slave should send messages with send_json method
            if msg['status'] == 'ok':
                await ws.close()
                break
            if msg['status'] == 'reg':
                ip = request.remote
                new_id = str(uuid4())
                while new_id in [slave['id'] for slave in self.slaves.values()]:
                    new_id = str(uuid4())
                await ws.send_json({'status': 'reg', 'id': new_id})
                self.slaves[ip] = dict()
                self.slaves[ip]['id'] = new_id
            elif msg['status'] == 'rm':
                ip = request.remote
                if (slave := self.slaves.get(ip)) and slave['id'] == msg['id']:
                    self.slaves.pop(request.remote)
                    await ws.send_json({'status': 'ok'})
        return ws

    async def current_instances(self, request: web.Request) -> web.Response:
        """returns all currently registered instances"""

        # ToDo - implement login logic
        # if request.can_read_body:
        #     body = await request.json()
        #
        #     # auth
        #     if (email := body.get('email')) and (password := body.get('password')):
        #         if await Database.check_user(self.conn, email, password):
        #             return web.json_response(self.slaves)
        #         else:
        #             return web.Response(status=423)
        #     else:
        #         return web.Response(status=401)
        return web.json_response(self.slaves)

    async def do(self, request: web.Request) -> web.Response:
        """
        sends request to the instance with IP to do something

        id:
            0 - return logs
            1 - update cache
        """

        if request.can_read_body:
            body = await request.json()

            todo = request.match_info.get('id')
            if 'ip' in body:  # execute for specific instance
                pass
            else:  # execute for all instances
                pass
        else:
            return web.Response(status=400)

    async def send_request(self, url: str, data: Dict) -> Dict:
        result = None
        # ToDo - add instance tokens
        async with ClientSession() as session:
            async with session.post(url) as response:
                if response.status == 200:
                    result = await response.json()
        return result


