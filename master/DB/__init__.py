import asyncpg as pg
import logging

from configparser import ConfigParser
from typing import Union
from asyncpg import Connection
from werkzeug.security import check_password_hash


class Database:

    @staticmethod
    async def create_conn(conf: ConfigParser) -> Union[pg.Connection, None]:
        """creates connection to the Database"""
        try:
            conn = await pg.connect(user=conf.get('postgres', 'user'), password=conf.get('postgres', 'pass'),
                                    database=conf.get('postgres', 'database'),
                                    host=conf.get('postgres', 'host'), port=conf.get('postgres', 'port'))
            return conn
        except Exception as e:
            logging.critical(str(e))

    @staticmethod
    async def check_user(conn: Connection, email: str, password: str) -> bool:
        """fetches user`s pass hash and account type and makes decision to auth or not"""
        query = """SELECT pass_hash, account_type FROM tiny_exchange.public.users
                   WHERE email = $1"""

        try:
            if res := await conn.fetch(query, email):
                if check_password_hash(res[0][0], password) and res[0][1] == 0:
                    return True
        except Exception as e:
            logging.error(str(e))
        return False
