FROM python:3.8

RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app/

COPY . /usr/src/app/

RUN pip install --no-cache-dir -r requirements.txt

ENV DEBUG=1
EXPOSE 8085
EXPOSE 5432

CMD ["python", "main.py"]